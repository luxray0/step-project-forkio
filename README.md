# Step-project 'Forkio'

## Команда проекту

- [Аліфіренко Антон](https://t.me/maskofsanity)
    - ## Виконав
        - [x] Шапка сайту з верхнім меню (включаючи випадаюче меню при малій роздільній здатності екрана)
        - [x] Секція `People Are Talking About Fork`
        - [x] header.js


- [Яременко Ян](https://t.me/yan_yaremenko)
    - ## Виконав
        - [x] Блок `Revolutionary Editor`
        - [x] Секція `Here is what you get`
        - [x] Секція `Fork Subscription Pricing`

## Технології

- [GulpJS](https://gulpjs.com/)
- [SASS](https://sass-lang.com/)
- [AOS](https://michalsnik.github.io/aos/)
- [HTML]()
- [JavaScript]()

### Вимоги

Для встановлення та запуску проекту, необхідний [NodeJS](https://nodejs.org/) v8+.

### Встановлення залежностей

Для встановлення залежностей, виконайте команду:

```sh
$ npm i
```

### Запуск Development сервера

Щоб запустити сервер для розробки, виконайте команду:

```sh
$ npm run dev
```

### Створення білда

Щоб виконати production збірку, виконайте команду:

```sh
$ npm run build
```
