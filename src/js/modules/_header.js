const getElementRefs = () => {
    return {
        hamburgerButton: document.querySelector('.header__hamburger'),
        hamburgerBox: document.querySelector('.header__hamburger-box'),
        hamburgerInner: document.querySelector('.header__hamburger-inner'),
        navList: document.querySelector('.header__nav'),
        brandLink: document.querySelector('.header__brand-link'),
        buyButton: document.querySelector('.header__btn--buy'),
        headerBody: document.querySelector('.header__body'),
        headerBodyWrapper: document.querySelector('.header__body--wrapper')
    };
};

const {
    hamburgerButton,
    hamburgerBox,
    hamburgerInner,
    navList,
    brandLink,
    buyButton,
    headerBody,
    headerBodyWrapper
} = getElementRefs();

const mediaMobile = window.matchMedia('(max-width: 480px)');

const toggleMenu = () => {
    hamburgerBox.classList.toggle('header__hamburger-box--active');
    hamburgerInner.classList.toggle('header__hamburger-inner--active');
    navList.classList.toggle('header__nav--active');
    document.body.classList.toggle('body--lock');
};

const handleHamburgerClick = () => {
    toggleMenu();
    if (!navList.classList.contains('header__nav--active')) {
        navList.style.transform = 'translateY(-100%)';
        return;
    }
    const headerNavActive = document.querySelector('.header__nav--active');
    const isScroll = document.documentElement.scrollTop > 120;
    headerNavActive.style.transform = `translateY(${isScroll ? '13%' : '27%'})`;
};

const handleBodyClick = (event) => {
    if (!navList.classList.contains('header__nav--active')) return;
    const isNav = [hamburgerButton, hamburgerBox, hamburgerInner, ...navList.querySelectorAll('.header__link')].some((el) => el === event.target);
    if (!isNav) {
        toggleMenu();
        navList.style.transform = 'translateY(-100%)';
    }
};

const changeHeaderStyle = () => {
    const isScrolled = document.documentElement.scrollTop > 120;
    brandLink.style.display = isScrolled ? 'none' : 'block';
    buyButton.style.display = isScrolled ? 'none' : 'block';
    headerBody.style.justifyContent = isScrolled ? 'flex-end' : 'space-between';
    headerBody.style.padding = isScrolled ? '0.875rem 0.3125rem 0.875rem 0.375rem' : '2rem 0.3125rem 1.875rem 0.375rem';
    headerBodyWrapper.style.setProperty('--headerBackground', isScrolled ? 'rgba(39, 38, 42, 50%)' : '#27262A');
};

hamburgerButton.addEventListener('click', handleHamburgerClick);
document.body.addEventListener('click', handleBodyClick);

if (mediaMobile.matches) {
    window.addEventListener('scroll', changeHeaderStyle);
}


